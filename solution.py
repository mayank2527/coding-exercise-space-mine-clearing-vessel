class Solution(object):
	"""docstring for Solution"""
	def __init__(self):
		self.input_field=[]
		self.fire_operations={
				'alpha':((-1, -1,0), (-1, 1,0), (1, -1,0), (1, 1,0)),
				'beta':((-1, 0,0), (0, -1,0), (0, 1,0), (1, 0,0)),
				'gamma':((-1, 0,0), (0, 0,0), (1, 0,0)),
				'delta':((0, -1,0), (0, 0,0), (0, 1,0))
			}
		self.firing_pattern=['alpha​','beta​','gamma​','delta​']
		self.firing_pattern=list(map(lambda x:x.replace(u'\u200b', ''),self.firing_pattern))
		self.move_pattern=['north','south','east','west']

	# print input field in console
	def print_input(self,input):
		for line in input:
			print(''.join(line))

	# return the position of all active mines
	def find_mines(self,field):
		mines_positions=[]
		for row,line in enumerate(field,0):
			for col,ch in enumerate(line,0):
				if ch!='.' and ch!='*':
					mines_positions.append((col,row,0))

		print(mines_positions)
		return mines_positions

	# return the position of all mines
	def find_all_mines(self,field):
		mines_positions=[]
		for row,line in enumerate(field,0):
			for col,ch in enumerate(line,0):
				if ch!='.':
					mines_positions.append((col,row,0))

		print(mines_positions)
		return mines_positions

	# regenerate the field after command applied
	def process_field(self,field,initial=False):
		mines_positions=solution.find_mines(field)
		X,Y,Z=self.vessel_position
		listX=[]
		listY=[]
		sizeX,sizeY,sizeZ=self.vessel_position
		for i,mine in enumerate(mines_positions,0):
			listX.append(mine[0])
			listY.append(mine[1])

		if(len(list(map(lambda x:abs(X-x),listX)))==0):
			sizeX=0
		else:
			sizeX=max(list(map(lambda x:abs(X-x),listX)))
		if(len(list(map(lambda y:abs(Y-y),listY)))==0):
			sizeY=0
		else:
			sizeY=max(list(map(lambda y:abs(Y-y),listY)))

		print(f"{sizeX} {sizeY}")
		new_vessel_position=[sizeX,sizeY,0]
		new_input_field=self.get_new_input_field(new_vessel_position,sizeX,sizeY,mines_positions,initial)

		self.input_field=new_input_field
		self.vessel_position=new_vessel_position

	# get the new field after operation
	def get_new_input_field(self,new_vessel_position,sizeX,sizeY,mines_positions,initial):
		new_input_field=[]
		for col in range(2*sizeY+1):
			input_row=[]
			for row in range(2*sizeX+1):
				input_row.append('.')
			new_input_field.append(input_row)

		print(new_input_field)
		diffX=new_vessel_position[0]-self.vessel_position[0]
		diffY=new_vessel_position[1]-self.vessel_position[1]

		if initial:
			for col,row,_ in mines_positions:
				new_input_field[row+diffY][col+diffX]=self.input_field[row][col]
		else:
			for col,row,_ in mines_positions:
				if self.input_field[row][col]=='a':
					new_input_field[row+diffY][col+diffX]='*'
				else:
					new_input_field[row+diffY][col+diffX]=chr(ord(self.input_field[row][col])-1)

		return new_input_field

	# check for remaining mines
	def has_mines(self):
		mines=self.find_mines(self.input_field)
		if len(mines)<1:
			return False
		else:
			return True

	# write the result at the end of file
	def print_result(self,initial_no_of_mines,shots_fired,no_of_moves,flag):
		mines=False if len(self.find_all_mines(self.input_field)) < 1 else True
		score=0
		if not mines:
			if flag:
				score=10*initial_no_of_mines-min([5*shots_fired,5*initial_no_of_mines])-min([2*no_of_moves,3*initial_no_of_mines])
			else:
				score=1
			with open("test.result",'a',encoding = 'utf-8') as f:
					f.write(f"Pass ({score})")
		else:
			with open("test.result",'a',encoding = 'utf-8') as f:
				f.write(f"Fail ({score})")

	# apply the fire operation
	def fire_torpedoes(self,fire):
		print(fire)	
		fire_positions=[tuple(map(sum, zip(tuple(self.vessel_position), x))) for x in self.fire_operations[fire[0]] ]
		print(fire_positions)
		self.print_input(self.input_field)

		for item in fire_positions:
			try:
				if self.input_field[item[1]][item[0]]!='*':
					self.input_field[item[1]][item[0]]='.'
			except IndexError:
				pass
		print('\n')
		self.print_input(self.input_field)

	# apply the move operation
	def move_vessel(self,move):
		if move[0]=='north':
			self.vessel_position[1]-=1
		elif move[0]=='south':
			self.vessel_position[1]+=1
		elif move[0]=='east':
			self.vessel_position[0]+=1
		elif move[0]=='west':
			self.vessel_position[0]-=1


	def write_output_1(self,index,script_cmd):
		with open("test.result",'a',encoding = 'utf-8') as f:
			f.write(f"step {index}")
			f.write('\n'*2)
			for line in self.input_field:
				f.write(''.join(line))
				f.write('\n')
			f.write('\n')
			f.write(' '.join(script_cmd))
			f.write('\n'*2)

	def write_output_2(self):
		with open("test.result",'a',encoding = 'utf-8') as f:
			for line in self.input_field:
				f.write(''.join(line))
				f.write('\n')
			f.write('\n')


solution=Solution()

with open("examples/test2.field",encoding = 'utf-8') as f:
	for line in f:
		solution.input_field.append(list(line.rstrip()))
	print(solution.input_field)

solution.vessel_position=[int(len(solution.input_field[0])/2),int(len(solution.input_field)/2),0]
solution.process_field(solution.input_field,True)

with open("examples/test2.script",encoding = 'utf-8') as f:
	initial_no_of_mines=len(solution.find_mines(solution.input_field))
	shots_fired=0
	no_of_moves=0
	flag=True
	for index,line in enumerate(f,1):
		if(not solution.has_mines()):
			flag=False
			break
		script_cmd=line.rstrip().split(' ')
		print(''.join(line))
		fire=[i for i in solution.firing_pattern if i in script_cmd]
		move=[i for i in solution.move_pattern if i in script_cmd]
		print(fire)
		solution.write_output_1(index,script_cmd)
		if len(fire)>0:
			shots_fired+=1
			solution.fire_torpedoes(fire)
		if len(move)>0:
			no_of_moves+=1
			solution.move_vessel(move)
		print(solution.vessel_position)
		solution.process_field(solution.input_field)
		solution.print_input(solution.input_field)
		# print(line.rstrip().split(' '))
		solution.write_output_2()

	solution.print_result(initial_no_of_mines,shots_fired,no_of_moves,flag)